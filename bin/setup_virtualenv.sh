#!/bin/bash -e

BASEDIR=`dirname $0`/..

if [ ! -d "$BASEDIR/.pyvirtualenv" ]; then
    virtualenv -q $BASEDIR/.pyvirtualenv --no-site-packages
    echo "Virtualenv created."
    
    source $BASEDIR/.pyvirtualenv/bin/activate
    
    pip install -r $BASEDIR/bin/requirements.txt
    echo "Requirements installed."
    
    deactivate
fi

