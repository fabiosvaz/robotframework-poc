#!/bin/bash -e

cd /usr/lib/chromium-browser/

#wget to fetch the version number of the latest release
LATEST=$(wget -q -O - http://chromedriver.storage.googleapis.com/LATEST_RELEASE)

#wget invocation in order to fetch the chromedriver build itself
wget http://chromedriver.storage.googleapis.com/$LATEST/chromedriver_linux64.zip

#Symlink chromedriver into /usr/local/bin/ so it's in your PATH and available system-wide:
unzip chromedriver_linux64.zip && sudo ln -s $PWD/chromedriver /usr/bin/chromedriver

rm chromedriver_linux64.zip
