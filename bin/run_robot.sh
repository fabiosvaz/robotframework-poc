#!/bin/bash

set -xe

BASEDIR=`dirname $0`/..


while [ $# -gt 0 ]
do
	case "$1" in
		-b)	browser="$2";shift;;
		-*)
			echo >&2 \
			"usage; $0 [-b firefox]"
			exit 1;;
		*)  break;;
	esac
	shift
done


cat << EOF
======================
Activating virtualenv
======================
EOF

source $BASEDIR/.pyvirtualenv/bin/activate


cat << EOF
======================
Starting Django server-
======================
EOF

nohup python $BASEDIR/sut/mysite/manage.py runserver --noreload &

PID=$!
echo $PID

cat << EOF
======================
Running Robot tests
======================
EOF

robot --variable BROWSER:$browser --outputdir $BASEDIR/results $BASEDIR/tests


cat << EOF
======================
Stopping django server
======================
EOF

kill $PID
ps aux | grep runserver

