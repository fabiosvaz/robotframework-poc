*** Settings ***
Documentation     A test suite for a valid login.
...
...               These are two styles applied to the following testcases

Resource          ../resources/admin_resource.robot
Test Teardown     Close Browser

Metadata    Version    1.0
Metadata    More Info      For more information about *Robot Framework* see http://robotframework.org
Metadata    Executed At    localhost

Default Tags    Req002 - Valid Login to Admin area


*** Test Cases ***
Valid Login Using Gherkin Style
    Given Browser Is Opened To Login Page
    When User "admin" logs in with password "adminadmin"
    Then Welcome Page Should Be Open

Log In Again Successfuly Using Gherkin Style
    Given Browser Is Opened To Login Page
    And User "admin" logs in with password "adminadmin"
    When Execute Logout
    And Log In Again
    Then User "admin" logs in with password "adminadmin"
    And Welcome Page Should Be Open
    
    
Valid Login
    Open Browser To Login Page
    Input Username    ${VALID USER}
    Input Password    ${VALID_PASSWORD}
    Submit Credentials
    Wait Until Page Does Not Contain Element    id_username
    Welcome Page Should Be Open
 

*** Keywords ***
Browser Is Opened To Login Page
    Open Browser To Login Page

User "${username}" logs in with password "${password}"
    Input username    ${username}
    Input password    ${password}
    Submit credentials
    Wait Until Page Does Not Contain Element    id_username    
