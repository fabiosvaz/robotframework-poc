*** Settings ***
Documentation     A test suite containing tests related to invalid login for the admin page.
...
...               These tests are data-driven by their nature. They use a single
...               keyword, specified with Test Template setting, that is called
...               with different arguments to cover different scenarios.
...
...               This suite also demonstrates using setups and teardowns in
...               different levels.
Suite Setup       Open Browser To Login Page
Suite Teardown    Close Browser

Test Setup        Go To Login Page
Test Template     Login With Invalid Credentials Should Fail
Resource          ../resources/admin_resource.robot

Metadata    Version    1.0
Metadata    More Info      For more information about *Robot Framework* see http://robotframework.org
Metadata    Executed At    localhost

Default Tags    Req001 - Invalid Login to Admin area

*** Test Cases ***               USER NAME        PASSWORD
Invalid Username                 invalid          ${VALID PASSWORD}
Invalid Password                 ${VALID USER}    invalid
Invalid Username And Password    invalid          whatever
Empty Username                   ${EMPTY}         ${VALID PASSWORD}
Empty Password                   ${VALID USER}    ${EMPTY}
Empty Username And Password      ${EMPTY}         ${EMPTY}

*** Keywords ***
Login With Invalid Credentials Should Fail
    [Arguments]    ${username}    ${password}
    Input Username    ${username}
    Input Password    ${password}
    Submit Credentials
    Login Should Have Failed    ${username}    ${password}

Login Should Have Failed
    [Arguments]    ${username}    ${password}
    Location Should Be    ${LOGIN URL}
    Run Keyword If  '${username}' != '${EMPTY}' and '${password}' != '${EMPTY}'     Username Or Password Invalid

Username Or Password Invalid
    Page Should Contain    Please enter the correct username and password for a staff account. Note that both fields may be case-sensitive.
