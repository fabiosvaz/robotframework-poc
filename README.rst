Robot Framework + Selenium2Library PoC 
=====================================================

.. contents::
   :local:


Introduction
------------

`Robot Framework`_ is a generic open source test
automation framework for acceptance testing and acceptance test-driven development (ATDD).

This PoC addresses Robot Framework test data syntax, how tests are executed, 
and how logs and reports look like.

The framework can be used with several existent test libraries, and this PoC is using `Selenium2Library`_.

New test libraries can be implemented either with Python or Java. See `Robot Framework User Guide`_
for more information.

This PoC will use a django demo application as a SUT.

Downloading robotframework-poc package
---------------------------------------

You can get the poc by cloning the `source code`_ or executing the command::

	git clone https://fabiosvaz@bitbucket.org/fabiosvaz/robotframework-poc.git
	
Alternatively you can download and extract the repository from the `download page`_


Preconditions
-------------

Linux
''''''

It's recommended to use a Linux environment. This poc was executed and tested in a Ubuntu 16.04.

Python2.7
'''''''''''

Requirements::

	 Python
	 Pip
	 Virtualenv

To check if you have the above requirements you can execute the following commands::

	python2.7 --version
	pip --version
	virtualenv --version

If your system does not have the above requirements, you can install them using::

	sudo apt-get install python2.7
	sudo apt-get install python-pip
	sudo pip install virtualenv


Python3.5
'''''''''''

Selenium2Library is not compatible with Python 3 (yet).	


Browsers
'''''''''

Chrome installation::

	sudo apt-get install chromium-browser

Firefox installation::

	sudo apt-get install firefox


Webdrivers
'''''''''

To manipulate browsers, WebDrivers/marionette proxy are required:

- `Firefox geckodriver`_
- `Chrome chromedriver`_

Once you have installed the Browsers, the following scripts can be used to install the drivers with sudo command:

- `</bin/install_mozila_geckodriver.sh>`_
- `</bin/install_chrome_chromedriver.sh>`_


Setup
-----

In order to run the poc, the following libraries need to be installed::

	Django
	robotframework
	robotframework-selenium2library

The `</bin/setup_virtualenv.sh>`_ can be used to create a virtualenv and install all of them for you.
The script will basically execute the commands::

	virtualenv -q $BASEDIR/.pyvirtualenv --no-site-packages
	source $BASEDIR/.pyvirtualenv/bin/activate
	pip install -r $BASEDIR/requirements.txt
	deactivate

Usage
------

In case you have installed the requirements in a virtual environment, you will need to activate it::

	source $BASEDIR/.pyvirtualenv/bin/activate

Manual execution
'''''''''''''''''

1. Open a command line and start the django server::

	cd $BASEDIR/sut/mysite
	python manage.py runserver

2. Open a second command line and run testcases with robot::

	## Executing a single test ##
	robot tests/admin_valid_login.robot --outputdir results
	
	## Executing all tests ##
	robot tests --outputdir results
	
	## Setting variables - Changing browser ##
	robot tests --variable BROWSER:Firefox --outputdir results

There are several options to run robot::

	robot --variable BROWSER:Firefox --name Firefox --log none --report none --output out/fx.xml login
	robot --variable BROWSER:IE --name IE --log none --report none --output out/ie.xml login
	
To see more more details, you can run robot --help or check the `Robot Framework User Guide<http://robotframework.org/robotframework/#user-guide>`_

Script execution
'''''''''''''''''

The existent script will start the django server in a background process and execute the robot tests.

1. Open a command line::

	cd $BASEDIR/bin
	bash run_robot.sh
	

Generated Results
--------------------------

After running robot tests you will get report and log in HTML format. Example
files are available as part of the source:

- `</results/report.html>`_
- `</results/log.html>`_


Learning Robot Framework
--------------------------

- `Robot Framework User Guide
  <http://robotframework.org/robotframework/#user-guide>`_
- `Standard libraries
  <http://robotframework.org/robotframework/#standard-libraries>`_
- `External libraries
  <http://robotframework.org/#test-libraries>`_
- `Built-in tools
  <http://robotframework.org/robotframework/#built-in-tools>`_
- `API documentation
  <http://robot-framework.readthedocs.org>`_
- `General documentation and demos
  <http://robotframework.org/#documentation>`_



.. _Robot Framework: http://robotframework.org
.. _Selenium2Library: https://github.com/robotframework/Selenium2Library
.. _Robot Framework User Guide: http://robotframework.org/robotframework/#user-guide
.. _download page: https://bitbucket.org/fabiosvaz/robotframework-poc/downloads
.. _source code: https://bitbucket.org/fabiosvaz/robotframework-poc/src
.. _Chrome chromedriver: https://sites.google.com/a/chromium.org/chromedriver
.. _Firefox geckodriver: https://github.com/mozilla/geckodriver
