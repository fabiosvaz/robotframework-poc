*** Settings ***
Documentation     A admin resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library
Resource          main_resource.robot

*** Variables ***
${ADMIN URL}      http://${SERVER}/admin/
${LOGIN URL}      http://${SERVER}/admin/login/
${VALID USER}     admin
${VALID PASSWORD}     adminadmin
${DELAY}          0


${WELCOME URL}    http://${SERVER}/welcome.html

*** Keywords ***
Open Browser To Login Page
    Open Browser    ${LOGIN URL}    ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed    ${DELAY}
    Login Page Should Be Open

Login Page Should Be Open
    Title Should Be    Log in | Django site admin

Go To Login Page
    Go To    ${LOGIN URL}
    Login Page Should Be Open

Input Username
    [Arguments]    ${username}
    Input Text    id_username    ${username}

Input Password
    [Arguments]    ${password}
    Input Text    id_password    ${password}

Submit Credentials
    Click Button    Log in

Welcome Page Should Be Open
    Location Should Be    ${ADMIN URL}
    Title Should Be    Site administration | Django site admin

Execute Logout
    Click Link    Log out
    Wait Until Page Contains    Thanks for spending some quality time with the Web site today.

Log In Again
    Click Link    Log in again
    Wait Until Page Contains Element    id_username