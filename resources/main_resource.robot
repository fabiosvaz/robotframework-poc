*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library

*** Variables ***
${SERVER}         localhost:8000
${BROWSER}        Firefox

*** Keywords ***
Open Browser To Specified Page
    [Arguments]    ${URL}
    Open Browser    ${URL}   ${BROWSER}



